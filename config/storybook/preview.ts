import type { Preview } from '@storybook/react';
import { StyleDecorator } from './decorators/StyleDecorator';
import { ThemeDecorator } from './decorators/ThemeDecorator';
import { RouterDecorator } from './decorators/RouterDecorator';

const preview: Preview = {
    parameters: {
        actions: { argTypesRegex: '^on[A-Z].*' },
        controls: {
            matchers: {
                color: /(background|color)$/i,
                date: /Date$/,
            },
        },
    },
    globalTypes: {
        theme: {
            description: 'Global theme for components',
            defaultValue: 'light',
            toolbar: {
                // The label to show for this toolbar item
                title: 'Theme',
                icon: 'circlehollow',
                // Array of plain string values or MenuItem shape (see below)
                items: ['app_light_theme', 'app_dark_theme'],
                // Change title based on selected value
                dynamicTitle: true,
            },
        },
    },
    decorators: [
        StyleDecorator,
        ThemeDecorator,
        RouterDecorator,
    ],
};

export default preview;
