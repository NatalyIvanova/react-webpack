import { Story, StoryContext } from '@storybook/react';
import { ThemeProvider } from '../../../src/app/providers/theme';

export const ThemeDecorator = (Story: Story, context: StoryContext) => {
    const { globals: { theme } } = context;

    return (
        <ThemeProvider defaultTheme={theme}>
            <div id="app" className={`app ${theme}`}>
                <Story />
            </div>
        </ThemeProvider>
    );
};
