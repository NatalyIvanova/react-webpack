import { classNames } from 'shared/lib/helpers/classNames';
import { Navbar } from 'widgets/navbar';
import { Sidebar } from 'widgets/sidebar';
import { Suspense } from 'react';
import { useTheme } from './providers/theme';
import { AppRouter } from './providers/router';
import cls from './App.module.scss';

export const App = () => {
    const { theme } = useTheme();

    return (
        <div id="app" className={classNames(cls.app, {}, ['app', theme])}>
            <Suspense fallback="">
                <Navbar />
                <div className={cls.pageContainer}>
                    <Sidebar />
                    <AppRouter pageContentClassName={cls.pageContent} />
                </div>
            </Suspense>
        </div>
    );
};
