import {
    createContext, FC, useMemo, useState,
} from 'react';

export const themeLSKey = 'theme';

export enum Theme {
    LIGHT = 'app_light_theme',
    DARK = 'app_dark_theme'
}

interface ThemeContextProps {
    theme: Theme;
    setTheme: (theme: Theme) => void;
}

const initialTheme = localStorage.getItem(themeLSKey) as Theme || Theme.LIGHT;

export const ThemeContext = createContext<Partial<ThemeContextProps>>({});

interface ThemeProviderProps {
    defaultTheme?: Theme;
}

export const ThemeProvider: FC<ThemeProviderProps> = ({ children, defaultTheme }) => {
    const [theme, setTheme] = useState<Theme>(defaultTheme || initialTheme);
    const initialThemeProps = useMemo(() => ({ theme, setTheme }), [theme]);

    return (
        <ThemeContext.Provider value={initialThemeProps}>
            {children}
        </ThemeContext.Provider>
    );
};
