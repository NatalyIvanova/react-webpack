import { Route, Routes } from 'react-router-dom';
import { Suspense } from 'react';
import { Loader } from 'shared/ui/loader';
import { routerConfig } from '../routerConfig';
import cls from './AppRouter.module.scss';

interface AppRouterProps {
    pageContentClassName?: string;
}

const AppRouter = ({ pageContentClassName }: AppRouterProps) => {
    const loaderComp = (
        <div className={cls.suspenseContainer}>
            <Loader className={cls.appRouterLoader} />
        </div>
    );

    return (
        <Suspense fallback={loaderComp}>
            <Routes>
                {Object.values(routerConfig).map(({ path, element }) => (
                    <Route
                        key={path}
                        path={path}
                        element={(
                            <div className={pageContentClassName}>
                                {element}
                            </div>
                        )}
                    />
                ))}
            </Routes>
        </Suspense>
    );
};

export default AppRouter;
