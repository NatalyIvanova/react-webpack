import { RouteProps } from 'react-router-dom';
import { HomePage } from 'pages/home';
import { AboutPage } from 'pages/about';
import { NotFoundPage } from 'pages/notFound';
import HomeIcon from 'shared/assets/icons/appLinks/home.svg';
import AboutIcon from 'shared/assets/icons/appLinks/about.svg';
import { ReactNode } from 'react';

enum AppRoutes {
    HOME = 'home',
    ABOUT = 'about',
    NOTFOUND = 'not_found'
}

type AppRoutesProps = RouteProps & {
    displayName: string,
    icon?: ReactNode
}

export const routerConfig: Record<AppRoutes, AppRoutesProps> = {
    [AppRoutes.HOME]: {
        path: '/',
        element: <HomePage />,
        displayName: 'home_page',
        icon: <HomeIcon />,
    },
    [AppRoutes.ABOUT]: {
        path: '/about',
        element: <AboutPage />,
        displayName: 'about_page',
        icon: <AboutIcon />,
    },
    [AppRoutes.NOTFOUND]: {
        path: '*',
        element: <NotFoundPage />,
        displayName: '',
    },
};
