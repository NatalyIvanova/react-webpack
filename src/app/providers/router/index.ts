import AppRouter from './ui/AppRouter';
import { routerConfig } from './routerConfig';

export { AppRouter, routerConfig };
