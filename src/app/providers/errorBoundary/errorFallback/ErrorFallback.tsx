import { classNames } from 'shared/lib/helpers/classNames';
import { useTranslation } from 'react-i18next';
import { VoidFunction } from 'app/types/types';
import { Button, ButtonStyle } from 'shared/ui/button';
import cls from './ErrorFallback.module.scss';

interface ErrorFallbackProps {
    className?: string;
    onReset?: VoidFunction;
}

export const ErrorFallback = ({ className, onReset = null }: ErrorFallbackProps) => {
    const { t } = useTranslation();

    return (
        <div className={classNames(cls.errorFallback, {}, [className])}>
            {t('default_error')}
            <Button onClick={onReset} className={cls.errorReset} mode={ButtonStyle.SCALE}>{t('reset_error')}</Button>
        </div>
    );
};
