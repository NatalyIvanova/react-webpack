import React, { ReactNode } from 'react';
import { Button } from 'shared/ui/button';
import { ErrorFallback } from 'app/providers/errorBoundary/index';

interface ErrorBoundaryProps {
    children: ReactNode;
    fallback?: ReactNode;
}

interface ErrorBoundaryState {
    hasError: Boolean;
}

export class ErrorBoundary extends React.Component<ErrorBoundaryProps, ErrorBoundaryState> {
    constructor(props: ErrorBoundaryProps) {
        super(props);
        this.state = { hasError: false };
        this.reset = this.reset.bind(this);
    }

    static getDerivedStateFromError(error: Error) {
        return { hasError: true };
    }

    componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
        console.log(error, errorInfo);
    }

    reset() {
        this.setState({ hasError: false });
    }

    render() {
        const { hasError } = this.state;
        const { children, fallback } = this.props;

        if (hasError) {
            return (
                <div>
                    {fallback || <ErrorFallback onReset={this.reset} /> }
                </div>
            );
        }
        return children;
    }
}
