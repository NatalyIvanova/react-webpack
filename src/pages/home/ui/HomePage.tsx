import { useTranslation } from 'react-i18next';

const HomePage = () => {
    const { t } = useTranslation();
    return (
        <div className="home-page">
            {t('home_page')}
        </div>
    );
};

export default HomePage;
