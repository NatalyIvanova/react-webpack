import { useTranslation } from 'react-i18next';

const AboutPage = () => {
    const { t } = useTranslation();
    return (
        <div className="about-page">
            {t('about_page')}
        </div>
    );
};

export default AboutPage;
