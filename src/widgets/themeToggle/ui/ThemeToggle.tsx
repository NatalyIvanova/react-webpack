import { classNames } from 'shared/lib/helpers/classNames';
import { useTheme } from 'app/providers/theme';
import Sunset from 'shared/assets/icons/sunset.svg';
import { ButtonHTMLAttributes } from 'react';
import { Button, ButtonStyle } from 'shared/ui/button';
import { useTranslation } from 'react-i18next';
import cls from './ThemeToggle.module.scss';

interface ThemeToggleProps extends ButtonHTMLAttributes<HTMLHtmlElement> {
    className?: string;
}

export const ThemeToggle = ({ className }: ThemeToggleProps) => {
    const { t } = useTranslation();
    const { toggleTheme } = useTheme();

    return (
        <Button
            aria-label={t('toggle_theme')}
            title={t('toggle_theme')}
            className={classNames('', {}, [className])}
            mode={ButtonStyle.SCALE}
            onClick={toggleTheme}
        >
            <Sunset className={cls.themeToggleIcon} />
        </Button>
    );
};
