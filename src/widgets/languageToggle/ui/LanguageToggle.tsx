import { classNames } from 'shared/lib/helpers/classNames';
import { ButtonHTMLAttributes } from 'react';
import { Button } from 'shared/ui/button';
import { useTranslation } from 'react-i18next';
import cls from './LanguageToggle.module.scss';

export enum AppLanguages {
    EN = 'en',
    RU = 'ru'
}

interface LanguageToggleProps extends ButtonHTMLAttributes<HTMLHtmlElement> {
    className?: string;
}

export const LanguageToggle = ({ className }: LanguageToggleProps) => {
    const { t, i18n } = useTranslation();

    const getNextLang = () => (
        i18n.language === AppLanguages.EN ? AppLanguages.RU : AppLanguages.EN
    );

    const onToggle = () => {
        i18n.changeLanguage(getNextLang());
    };

    return (
        <Button
            className={classNames(cls.languageToggle, {}, [className])}
            title={`${t('change_to')} ${getNextLang()}`}
            onClick={onToggle}
        >
            {i18n.language}
        </Button>
    );
};
