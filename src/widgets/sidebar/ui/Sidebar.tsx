import { classNames } from 'shared/lib/helpers/classNames';
import { useState } from 'react';
import { ThemeToggle } from 'widgets/themeToggle';
import { LanguageToggle } from 'widgets/languageToggle';
import { Button } from 'shared/ui/button';
import { useTranslation } from 'react-i18next';
import DoubleArrow from 'shared/assets/icons/doubleArrow.svg';

import { routerConfig } from 'app/providers/router';
import { AppLink } from 'shared/ui/appLink';
import cls from './Sidebar.module.scss';

interface SidebarProps {
    className?: string;
}

export const Sidebar = ({ className }: SidebarProps) => {
    const { t } = useTranslation();
    const [isCollapsed, setIsCollapsed] = useState(false);
    const sidebarToggleName = isCollapsed ? 'open_sidebar' : 'close_sidebar';

    const onToggle = () => {
        setIsCollapsed((prevState) => !prevState);
    };

    return (
        <div
            data-testid="sidebar"
            className={classNames(cls.sidebar, { [cls.collapsed]: isCollapsed }, [className])}
        >
            {Object.values(routerConfig).map(({ path, displayName, icon }) => displayName && (
                <AppLink className={cls.sidebarLink} key={path} to={path}>
                    <span className={cls.sidebarLinkIcon} title={isCollapsed ? t(displayName) : null}>{icon}</span>
                    <span className={cls.sidebarLinkText}>{t(displayName)}</span>
                </AppLink>
            ))}
            <div className={cls.sidebarFooter}>
                <ThemeToggle />
                <LanguageToggle />
                <Button
                    data-testid="sidebarToggle"
                    className={cls.sidebarToggle}
                    title={t(sidebarToggleName)}
                    onClick={onToggle}
                >
                    <DoubleArrow className={cls.sidebarToggleIcon} />
                </Button>
            </div>
        </div>
    );
};
