import type { Meta, Story } from '@storybook/react';
import { Sidebar } from 'widgets/sidebar';

const meta = {
    title: 'widgets/Sidebar',
    component: Sidebar,
    parameters: {
        layout: 'centered',
    },
    tags: ['autodocs'],
    decorators: [
        (Story: Story) => (
            <div style={{ display: 'flex', width: '84vw', height: '70vh' }}>
                <Story />
                <div style={{ flexGrow: 1 }} />
            </div>
        ),
    ],
} satisfies Meta<typeof Sidebar>;

export default meta;

export const Default = {
    args: {},
};
