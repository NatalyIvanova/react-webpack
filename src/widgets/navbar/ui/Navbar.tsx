import { classNames } from 'shared/lib/helpers/classNames';
import { ThemeToggle } from 'widgets/themeToggle';
import { useTranslation } from 'react-i18next';
import { useState } from 'react';
import { Button, ButtonStyle } from 'shared/ui/button';
import { LoginModal } from 'features/authByUserName';
import cls from './Navbar.module.scss';

interface NavbarProps {
    className?: string;
}

export const Navbar = ({ className }: NavbarProps) => {
    const { t } = useTranslation();
    const [isModalOpen, setModalOpen] = useState(false);

    return (
        <div className={classNames(cls.navbar, {}, [className])}>
            <ThemeToggle className={cls.themeToggle} />
            <Button type="button" mode={ButtonStyle.OUTLINED} onClick={() => setModalOpen(true)}>
                {t('login')}
            </Button>
            {/* eslint-disable-next-line i18next/no-literal-string */}
            <LoginModal isOpen={isModalOpen} onClose={() => setModalOpen(false)} />
        </div>
    );
};
