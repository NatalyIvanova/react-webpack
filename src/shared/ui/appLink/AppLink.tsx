import { classNames } from 'shared/lib/helpers/classNames';
import { Link, LinkProps } from 'react-router-dom';
import cls from './AppLink.module.scss';

interface AppLinkProps extends LinkProps {
    className?: string;
}

export const AppLink = (props: AppLinkProps) => {
    const {
        to, className, children, ...otherProps
    } = props;
    return (
        <Link
            to={to}
            className={classNames(cls.appLink, {}, [className])}
            {...otherProps}
        >
            {children}
        </Link>
    );
};
