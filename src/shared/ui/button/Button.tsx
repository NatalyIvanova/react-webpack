import { classNames } from 'shared/lib/helpers/classNames';
import { ButtonHTMLAttributes } from 'react';
import cls from './Button.module.scss';

export enum ButtonStyle {
    SCALE = 'scale',
    BOLD = 'bold',
    PRIMARY = 'primary',
    OUTLINED = 'outlined'
}

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement>{
    className?: string;
    mode?: ButtonStyle;
}

export const Button = (props: ButtonProps) => {
    const {
        className, mode, children, ...otherProps
    } = props;
    return (
        <button
            type="button"
            className={classNames(cls.button, { [cls[mode]]: true }, [className])}
            {...otherProps}
        >
            {children}
        </button>
    );
};
