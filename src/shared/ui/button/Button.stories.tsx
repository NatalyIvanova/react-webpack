import type { Meta, StoryObj } from '@storybook/react';

import { Button, ButtonStyle } from './Button';

const meta = {
    title: 'shared/Button',
    component: Button,
    parameters: {
        layout: 'centered',
    },
    tags: ['autodocs'],
} satisfies Meta<typeof Button>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Scale: Story = {
    args: {
        mode: ButtonStyle.SCALE,
        children: 'Button',
    },
};

export const Bold: Story = {
    args: {
        mode: ButtonStyle.BOLD,
        children: 'Bold',
    },
};

export const Primary: Story = {
    args: {
        mode: ButtonStyle.PRIMARY,
        children: 'Primary',
    },
};

export const Outlined: Story = {
    args: {
        mode: ButtonStyle.OUTLINED,
        children: 'Outlined',
    },
};
