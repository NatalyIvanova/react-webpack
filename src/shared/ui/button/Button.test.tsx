import { render, screen } from '@testing-library/react';
import { Button, ButtonStyle } from 'shared/ui/button/Button';

describe('Button', () => {
    test('basic', () => {
        render(<Button>Test</Button>);
        expect(screen.getByText('Test')).toBeInTheDocument();
    });
    test('with .scale', () => {
        render(<Button mode={ButtonStyle.SCALE}>Test</Button>);
        expect(screen.getByText('Test')).toHaveClass('scale');
        // screen.debug();
    });
    test('with .bold', () => {
        render(<Button mode={ButtonStyle.BOLD}>Test</Button>);
        expect(screen.getByText('Test')).toHaveClass('bold');
    });
    test('with .primary', () => {
        render(<Button mode={ButtonStyle.PRIMARY}>Test</Button>);
        expect(screen.getByText('Test')).toHaveClass('primary');
    });
    test('with .outlined', () => {
        render(<Button mode={ButtonStyle.OUTLINED}>Test</Button>);
        expect(screen.getByText('Test')).toHaveClass('outlined');
    });
});
