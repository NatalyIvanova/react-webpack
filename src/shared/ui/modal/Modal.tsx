import { classNames } from 'shared/lib/helpers/classNames';
import React, {
    ReactNode, useState, useRef, useEffect, useCallback,
} from 'react';
import { VoidFunction } from 'app/types/types';
import { Portal } from 'shared/ui/portal';
import { current } from '@reduxjs/toolkit';
import cls from './Modal.module.scss';

const ANIMATION_DELAY = 300;

export interface ModalProps {
    className?: string;
    children: ReactNode;
    isOpen: boolean;
    onClose: VoidFunction;
    isLazy?: boolean;
}

export const Modal = (props: ModalProps) => {
    const {
        className, children, isOpen, onClose, isLazy,
    } = props;
    const [isClosing, setIsClosing] = useState(false);
    const [isMounted, setIsMounted] = useState(false);
    const timerRef = useRef<ReturnType<typeof setTimeout>>();
    const modalParentRef = document.getElementById('app') || document.body;
    const mods: Record<string, boolean> = {
        [cls.open]: isOpen,
        [cls.isClosing]: isClosing,
    };

    useEffect(() => {
        if (isOpen) {
            setIsMounted(true);
        }
    }, [isOpen]);

    const handleClose = useCallback(() => {
        setIsClosing(true);
        timerRef.current = setTimeout(() => {
            onClose();
            setIsClosing(false);
        }, ANIMATION_DELAY);
    }, [onClose]);

    const onKeyDown = useCallback((e: KeyboardEvent) => {
        if (e.key === 'Escape') {
            handleClose();
        }
    }, [handleClose]);

    useEffect(() => {
        if (isOpen) {
            window.addEventListener('keydown', onKeyDown);
        }

        return () => {
            clearTimeout(timerRef.current);
            window.removeEventListener('keydown', onKeyDown);
        };
    }, [isOpen, onKeyDown]);

    const onContentClicked = (e: React.MouseEvent) => {
        e.stopPropagation();
    };

    if (isLazy && !isMounted) {
        return null;
    }

    return modalParentRef ? (
        <Portal element={modalParentRef}>
            <div className={classNames(cls.modal, mods, [className])}>
                <div className={cls.overlay} onClick={handleClose}>
                    <div className={cls.content} onClick={onContentClicked}>
                        {children}
                    </div>
                </div>
            </div>
        </Portal>
    ) : null;
};
