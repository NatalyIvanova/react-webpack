import type { Meta, StoryObj } from '@storybook/react';
import { Theme } from 'app/providers/theme';
import { Modal } from './Modal';
import { ThemeDecorator } from '../../../../config/storybook/decorators/ThemeDecorator';

const meta = {
    title: 'shared/Modal',
    component: Modal,
    parameters: {
        layout: 'centered',
    },
    tags: ['autodocs'],
} satisfies Meta<typeof Modal>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Light: Story = {
    args: {
        children: 'This is a modal window.',
        isOpen: true,
    },
};

export const Dark: Story = {
    args: {
        children: 'This is a modal window.',
        isOpen: true,
    },
};
// Dark.decorators = [ThemeDecorator(Theme.DARK)];
