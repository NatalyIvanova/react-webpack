import { classNames } from 'shared/lib/helpers/classNames';
import LoaderSvg from 'shared/assets/loader.svg';

interface LoaderProps {
    className?: string;
}

export const Loader = ({ className }: LoaderProps) => (
    <div className={classNames('loader', {}, [className])}>
        <LoaderSvg />
    </div>
);
