type Modes = Record<string, boolean | string>;

export const classNames = (main: string, modes: Modes = {}, additional: string[] = []) => [
    main,
    ...additional.filter(Boolean),
    ...Object.entries(modes)
        .filter(([_, value]) => !!value)
        .map(([className]) => className),
].join(' ');
