import { ReactNode } from 'react';
import { render } from '@testing-library/react';
import { I18nextProvider } from 'react-i18next';
import i18nTests from 'shared/lib/tests/i18nTests';
import { MemoryRouter } from 'react-router-dom';
import { StoreProvider } from 'app/providers/store';
import { DeepPartial } from '@reduxjs/toolkit';
import { StateSchema } from 'app/providers/store/config/StateSchema';

interface ComponentRenderOptions {
    route?: string;
    initialState?: DeepPartial<StateSchema>;
}

export const componentRender = (component: ReactNode, options: ComponentRenderOptions = {}) => {
    const { route = '/', initialState } = options;
    return render(
        <StoreProvider initialState={initialState as StateSchema}>
            <MemoryRouter initialEntries={[route]}>
                <I18nextProvider i18n={i18nTests}>
                    {component}
                </I18nextProvider>
            </MemoryRouter>
        </StoreProvider>,

    );
};
