import { counterReducer, counterActions } from './counter.slice';
import { CounterSchema } from '../types/CounterSchema';

describe('counterSlice.test', () => {
    test('increment counter', () => {
        const state: CounterSchema = { value: 10 };
        expect(counterReducer(state, counterActions.increment())).toEqual({ value: 11 });
    });

    test('decrement counter', () => {
        const state: CounterSchema = { value: 10 };
        expect(counterReducer(state, counterActions.decrement())).toEqual({ value: 9 });
    });

    test('empty state case', () => {
        expect(counterReducer(undefined, counterActions.increment())).toEqual({ value: 1 });
    });
});
