import { createSelector } from '@reduxjs/toolkit';
import { counterState } from './counterState';
import { CounterSchema } from '../types/CounterSchema';

export const counterValue = createSelector(counterState, (counter: CounterSchema) => counter.value);
