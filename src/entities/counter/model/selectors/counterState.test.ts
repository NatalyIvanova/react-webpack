import { DeepPartial } from '@reduxjs/toolkit';
import { StateSchema } from 'app/providers/store/config/StateSchema';
import { counterState } from './counterState';

describe('counterState', () => {
    test('get counter slice from state', () => {
        const state: DeepPartial<StateSchema> = {
            counter: {
                value: 10,
            },
        };
        expect(counterState(state as StateSchema)).toEqual({ value: 10 });
    });
});
