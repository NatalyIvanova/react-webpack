import { StateSchema } from 'app/providers/store/config/StateSchema';

export const counterState = (state: StateSchema) => state.counter;
