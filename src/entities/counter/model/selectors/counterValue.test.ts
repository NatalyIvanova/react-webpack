import { DeepPartial } from '@reduxjs/toolkit';
import { StateSchema } from 'app/providers/store/config/StateSchema';
import { counterValue } from './counterValue';

describe('counterValue', () => {
    test('get counter value from counter slice', () => {
        const state: DeepPartial<StateSchema> = {
            counter: {
                value: 10,
            },
        };
        expect(counterValue(state as StateSchema)).toEqual(10);
    });
});
