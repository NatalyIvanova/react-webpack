import { componentRender } from 'shared/lib/tests/componentRender';
import { fireEvent, screen } from '@testing-library/react';
import { Counter } from './Counter';

describe('counter.test', () => {
    test('is counter has value', () => {
        componentRender(<Counter />, { initialState: { counter: { value: 10 } } });
        expect(screen.getByTestId('counter-value')).toHaveTextContent('10');
    });

    test('counter increment', () => {
        componentRender(<Counter />, { initialState: { counter: { value: 10 } } });
        fireEvent.click(screen.getByTestId('counter-increment-btn'));
        expect(screen.getByTestId('counter-value')).toHaveTextContent('11');
    });

    test('counter decrement', () => {
        componentRender(<Counter />, { initialState: { counter: { value: 10 } } });
        fireEvent.click(screen.getByTestId('counter-decrement-btn'));
        expect(screen.getByTestId('counter-value')).toHaveTextContent('9');
    });
});
