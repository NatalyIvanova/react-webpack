import { Button } from 'shared/ui/button';
import { useDispatch, useSelector } from 'react-redux';
import { counterActions } from 'entities/counter/model/slice/counter.slice';
import { useTranslation } from 'react-i18next';
import { counterValue } from '../model/selectors/counterValue';

export const Counter = () => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const counter = useSelector(counterValue);

    const handleIncrement = () => {
        dispatch(counterActions.increment());
    };

    const handleDecrement = () => {
        dispatch(counterActions.decrement());
    };

    return (
        <div>
            <p data-testid="counter-value">{counter}</p>
            <Button data-testid="counter-increment-btn" onClick={handleIncrement}>
                {t('increment')}
            </Button>
            <Button data-testid="counter-decrement-btn" onClick={handleDecrement}>
                {t('decrement')}
            </Button>
        </div>
    );
};
