import { Modal, ModalProps } from 'shared/ui/modal';
import { LoginForm } from '../loginForm/LoginForm';

interface LoginModalProps extends Omit<ModalProps, 'children'> {
    className?: string;
}

export const LoginModal = ({ className, ...props }: LoginModalProps) => {
    const compName = 'login-form';

    return (
        <Modal isLazy className={compName} {...props}>
            <LoginForm />
        </Modal>
    );
};
