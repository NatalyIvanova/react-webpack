import { classNames } from 'shared/lib/helpers/classNames';
import { useTranslation } from 'react-i18next';
import { Button, ButtonStyle } from 'shared/ui/button';
import { Input } from 'shared/ui/input/Input';
import { useDispatch, useSelector } from 'react-redux';
import { loginActions } from 'features/authByUserName/model';
import { memo, useCallback } from 'react';
import { selectLoginState } from 'features/authByUserName/model/selectors/selectLoginState';
import cls from './LoginForm.module.scss';

interface LoginFormProps {
    className?: string;
}

export const LoginForm = memo(({ className }: LoginFormProps) => {
    const compName = 'login-form';
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const { username, password } = useSelector(selectLoginState);

    const handleChangeUsername = useCallback((value: string) => {
        dispatch(loginActions.setUserName(value));
    }, [dispatch]);

    const handleChangePassword = useCallback((value: string) => {
        dispatch(loginActions.setPassword(value));
    }, [dispatch]);

    return (
        <div className={classNames(cls.loginForm, {}, [className])}>
            <Input
                id={`${compName}_${t('username')}`}
                value={username}
                label={t('username')}
                autofocus
                className={cls.loginInput}
                onChange={handleChangeUsername}
            />
            <Input
                type="text"
                value={password}
                label={t('password')}
                className={cls.loginInput}
                onChange={handleChangePassword}
            />
            <Button className={cls.loginBtn} mode={ButtonStyle.PRIMARY}>{t('login')}</Button>
        </div>
    );
});
