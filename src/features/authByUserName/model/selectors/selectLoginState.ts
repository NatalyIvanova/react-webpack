import { StateSchema } from 'app/providers/store/config/StateSchema';

export const selectLoginState = (state: StateSchema) => state.login;
